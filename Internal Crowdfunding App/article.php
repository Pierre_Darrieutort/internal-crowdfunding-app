<?php

session_start();

include_once('php/functions.php');

?>
<style type="text/css">
     @font-face {
      font-family: "Trade Gothic LT Std";
      src: url("<?php echo $websiteURL; ?>fonts/TradeGothicLTStd.otf");
    }

   @font-face {
      font-family: "Trade Gothic LT Std Bold";
      src: url("<?php echo $websiteURL; ?>fonts/TradeGothicLTStd-Bold.otf");
    }
</style>
<?php


if(isset($_GET['id']) AND !empty($_GET['id'])) {
   $get_id = htmlspecialchars($_GET['id']);
   $article = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
   $article->execute(array($get_id));
   if($article->rowCount() == 1) {
      $article = $article->fetch();
      $titre = $article['titre'];
      $contenu = $article['contenu'];
      $recolte = $article['recolte'];
      $articleID = $article['id'];
      $pseudo = $article['pseudo'];
      $infomembres = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
      $infomembres->execute(array($_SESSION['id']));
      $infoM = $infomembres->fetch();
      $coins = $infoM['coins'];
      //var_dump($article);
      //var_dump($infoM);
      //var_dump($_SESSION['id']);
      //var_dump($article['pseudo']);










      $commentaires = $bdd->prepare('SELECT * FROM commentaires WHERE id_article = ? ORDER BY date_time_post DESC');
      $commentaires->execute(array($get_id));

      if(isset($_POST['submit_commentaire'])) {
         if(isset($_SESSION['pseudo'],$_POST['commentaire']) AND !empty($_SESSION['pseudo']) AND !empty($_POST['commentaire'])){
            $pseudoPost = htmlspecialchars($_SESSION['pseudo']);
            $commentaire = htmlspecialchars($_POST['commentaire']);
            if (strlen($pseudoPost) < 50) {
               $ins = $bdd->prepare('INSERT INTO commentaires (pseudo, commentaire, id_article, don, date_time_post) VALUES (?, ?, ?, 0, NOW())');
               $ins->execute(array($pseudoPost, $commentaire, $get_id));
               $c_msg = "Votre commentaire a bien été posté";
               $commentaires->execute(array($get_id));//répétition de l'exécution de la requete afin de rafraichir les commentaires.
            } else {
               $c_msg = "Erreur: Le nom d'utilisateur doit faire moins de 25 caractères";
            }
         } else {
            $c_msg = "Erreur: Tous les champs doivent être complétés!";
         }
      }



   }else{
      die('Cet article n\'existe pas !');
   }

}else{
   header("Location: actualites.php");
}

//var_dump($article);
//$pseudo = $article['pseudo'];




if(isset($_POST['soutien'])) {
   $montantfinalhidden = (float)$_POST['montantfinalhidden'];

   if ( $montantfinalhidden < 0 ) {
     $c_msg = "<font color='red'>vous ne pouvez pas mettre de valeur négative!</font>";
   } else if ( $montantfinalhidden == 0 ) {
      $c_msg = "<script>console.log('Les dons de 0,00 &hearts; sont absolument inutiles...')</script>";
   } else if ( $montantfinalhidden > $coins ) {
      $c_msg = "<font color='red'>Vous n'avez pas assez de coins!</font>";
   } else {
      #tout semble bien se passer...
      $resultatclient = $coins - $montantfinalhidden;
      $resultatprojet = $recolte + $montantfinalhidden;

      $accountupdate_projet = $bdd->prepare('UPDATE articles SET recolte = '.$resultatprojet.' WHERE id = '.$articleID);
      $accountupdate_client = $bdd->prepare('UPDATE membres SET coins = '.$resultatclient.' WHERE id ='.$_SESSION['id']);

      $accountupdate_projet->execute(array());
      $accountupdate_client->execute(array());

      $recolte = $resultatprojet;
      $coins = $resultatclient;


      //affichage du don dans la partie commentaires:
      $don = $montantfinalhidden;
      $doncomment = " a fait un don de ".$don." &hearts;";
      $ins = $bdd->prepare('INSERT INTO commentaires (pseudo, commentaire, id_article, don, amountdon, date_time_post) VALUES (?, ?, ?, 1, ?, NOW())');
      $ins->execute(array($_SESSION['pseudo'], $doncomment, $get_id, $don));
      $commentaires->execute(array($get_id));
   }
}




if(isset($_SESSION['id'])) {


$bigtitle = $titre;// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/articles.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>
<antibody>
   <!--<a href="actualites.php"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><br>-->
   <h2><?php echo $titre ?></h2>
   <h4>Rédigé par: <?php echo $article['pseudo'] ?></h4>
   <p><?= $contenu ?></p>


<?php
//utilisé pour obtenir les infos telles que le pseudo, si l'user est moderateur ou si c'est lui qui a rédigé l'article.
$getid = intval($_SESSION['id']);//transforme en chiffre l'id de session (variable devient chiffre).
      $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
      $requser->execute(array($getid));//exécute la commande au dessus
      $userinfo = $requser->fetch();//va chercher et attribue les infos utilisateurs de 'requser' à 'userinfo' dans un tableau affichable avec la commande "var_dump".
      //var_dump($userinfo);


$pseudo = $userinfo['pseudo'];
$moderator = $userinfo['moderator'];




if(isset($_GET['id']) AND !empty($_GET['id'])) {
   $froz_id = htmlspecialchars($_GET['id']);
   $reqOwnArticle = $bdd->prepare('SELECT status FROM articles WHERE id = ?');
   $reqOwnArticle->execute(array($froz_id));
   $statusinfo = $reqOwnArticle->fetch();
   $status = $statusinfo[0];
   //var_dump($status);
}


?>


   <?php if ($status == 2) : //bouclage ?>
      <br><h3>Ce projet a atteint la somme nécéssaire pour être réalisé.</h3>
   <?php elseif ($status == 3) : //suspension ?>
      <br><h3>Ce projet a été suspendu et aucune somme ne peut plus lui être alloué.</h3>
   <?php else : ?>
      <?php if( $pseudo === 'Grand Chef' OR $moderator == 1 OR $article['pseudo'] == $_SESSION['pseudo'] ) : ?>
      <center class="frozenProject">
         <a class="faperso1" href="frozenProject/bouclage.php?id=<?= $get_id ?>" onclick="return(confirm('Ce projet a reçu une somme suffisante et va être bouclé. Aucun fond supplémentaire ne pourra y être alloué. Continuer ?'));"><i class="fa fapadd fa-trophy" aria-hidden="true"></i>Boucler ce projet</a>
         <br><br>
         <a class="faperso2" href="frozenProject/suspension.php?id=<?= $get_id ?>" onclick="return(confirm('Ce projet va être suspendu et aucun fond supplémentaire ne pourra y être alloué. Continuer ?'));"><i class="fa fapadd fa-power-off" aria-hidden="true"></i>Suspendre ce projet</a>
      </center>
      <?php else : ?>
         <br>
      <?php endif ?>
   <p>Ce projet a récolté: <?= $recolte ?> <img src="images/coin(gh).png" style="width:20px;position:relative;top:3.5px;"></p>
   <p class="frozenProject">Vous avez <?='<font color="green">'.$coins."</font>"?> <img src="images/coin(gh).png" style="width:20px;position:relative;top:3.5px;"> sur votre compte</p>



   <form class="frozenProject" id="montantform" method="POST" action="" onSubmit="if(!confirm('le montant de votre don est de: ' + document.getElementById('amount').value+'&hearts;')){return false;}">
      <!--<input type="text" name="lemontant" id="amount"  />-->
      <input required="required" type="number" class="inepoute" class="centerholder" name="lemontant" id="amount"  min="1" max="100.00" step="1" placeholder="exemple: 15 &hearts;" style="text-align: center;"/>
      <input type="hidden" id="montantfinalhidden" name="montantfinalhidden" value="0">
      <br>
      <input type="submit" class="ideaenvoi" id="envoi_soutien" name="soutien" value="Soutenir ce projet" onClick="Montant();">
   </form>



   <h3>COMMENTAIRES:</h3>
   <form method="POST" class="frozenProject">
   <input type="hidden" name="pseudo" value="<?= $avatar_result['pseudo']; ?>">
   <textarea name="commentaire" class="inepoute" placeholder="Votre commentaire" required="required"></textarea><br>
   <input type="submit" class="ideaenvoi" value="Envoyer" name="submit_commentaire">
   </form><br>
   <?php if (isset($c_msg)) { echo "<center>".$c_msg."</center>"; } ?>


   <?php endif //si le projet est en frozen status, alors les éléments de commentaire et don disparraissent pour laisser place à un message. ?>


   <br>
   <?php while($c = $commentaires->fetch()) { ?>

        <?php
		  $req_avatar = $bdd->prepare('SELECT * FROM membres WHERE pseudo = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
	      $req_avatar->execute(array($c['pseudo']));
	      $avatar_result = $req_avatar->fetch();
	      //var_dump($avatar_result);
	    ?>

   <ul class="comm_dons">
   <?php if ( $c['don'] == 0 ) : ?>

      <li class="padon padon-fond">
         <a class="pseudalink" href="profil.php?id=<?= $avatar_result['id'] ?>">
           <img class="icon-comment" src="images/membres/avatars/<?= $avatar_result['avatar'] ?>">
           <span><?php echo  $c['pseudo'] ?>: </span>
         </a>
         <div class="mycomm"><?= $c['commentaire']; ?></div>
      </li>

   <?php elseif ( $c['don'] == 1 ) : ?>

     <li class="don don-fond">
      <img class="icon-donation" src="images/money.gif"><div class="comm_donation">
      <a class="pseudalink" href="profil.php?id=<?= $avatar_result['id'] ?>">
         <?php echo  $c['pseudo'] ?>
      </a>
      <?= $c['commentaire']; ?></div>
     </li>
      
   <?php else : ?>

      <li>la valeur du don ne peut être non-boolléene</li>
   

   <?php endif ?>
     
   </ul>

   <?php
   }
   ?>





</antibody>
<?php include_once('php/pre-bottom.php'); ?>
<script>
   function Montant()
        {
            var montant = document.getElementById("amount").value;
            console.log(montant);
            var montantFinal = parseFloat(montant.replace(",", "."));
            console.log(montantFinal);
            var changehiddenvalue = montantFinal;
            document.getElementById("montantfinalhidden").value = changehiddenvalue;
            console.log(changehiddenvalue);
            if ( montant == "0.00 &hearts;" ){
               document.getElementById("amount").value = "";
               return false;
            } else if (montant == "" ) {
               return false;
            } else {
               var confirmation = console.log('Le montant de ce don est de: '+montant+'&hearts;');
                var txt;
                    txt = "You pressed OK!";
                    console.log(txt);
            }
        }      
</script>
<?php
include_once('php/bottom.php');//</body>...
           
}//termine la vérification de session commencée au 'if(isset($_SESSION['id'])){'
else{
   header("Location: connexion.php");
}
?>