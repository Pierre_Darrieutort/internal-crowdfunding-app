<?php
session_start();//permet de lancer une instance temporaire permetant d'ajouter des variables de session

include_once('php/functions.php');
include_once('cookieconnect.php');


if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
   


   header("Location: profil.php?id=".$_SESSION['id']);
}//si l'id dans le cookie de session existe ET qu'il est supérieur à zéro, ALORS direction la page de profil d'utilisateur car cela signifie qu'il est déjà connecté alors il n'a pas besoin d'accéder à cette page !


else if(isset($_POST['formconnexion'])) {
   $pseudoconnect = htmlspecialchars($_POST['pseudoconnect']);//sécurisation du champ
   $mdpconnect = sha1(htmlspecialchars($_POST['mdpconnect']));//décodage du mot de passe dans le champ
   if(!empty($pseudoconnect) AND !empty($mdpconnect)) {//si les deux champs sont vides
         $requser = $bdd->prepare("SELECT * FROM membres WHERE pseudo = ? AND motdepasse = ?");//se prépare à aller chercher dans la BDD le mail et mot de passe
      $requser->execute(array($pseudoconnect, $mdpconnect));//exécute la commande au dessus
      $userexist = $requser->rowCount();//compte le nombre de rangées existant avec le même mail et mot de passe
      if($userexist == 1) {//si il n'y a strictement qu'une ligne qui correspond


         

         if(isset($_POST['rememberme'])) {
            setcookie('pseudo',$pseudoconnect,time()+365*24*3600,null,null,false,true);
            setcookie('password',$mdpconnect,time()+365*24*3600,null,null,false,true);
            setcookie('password',$mdpconnect,time()+365*24*3600,null,null,false,true);
         }


         $userinfo = $requser->fetch();//'userinfo' récupère(fetch) la ligne correspodante au mail et mdp au dessus
         $_SESSION['id'] = $userinfo['id'];//attribue le contenu de la case ID de la ligne correspondante au mail et mdp du dessus
         $_SESSION['pseudo'] = $userinfo['pseudo'];//attribue la case PSEUDO de la ligne correspondante au mail et mdp au dessus
         $_SESSION['mail'] = $userinfo['mail'];//attribue la case MAIL de la ligne correspondante au mail et mdp au dessus
         
#AJOUT STATUS CONNECTION_START#
         $updateStatus1 = $bdd->prepare( 'UPDATE membres SET status = ? WHERE id = ?' );
         $updateStatus1->execute(array( 1, $_SESSION['id'] ) );
#AJOUT STATUS CONNEXION_END#


         header("Location: profil.php?id=".$_SESSION['id']);//lance une redirection vers la page 'profil.php' avec dans l'url l'ajout d'une variable de session qui est l'ID correspondant au mail et mdp au dessus


      } else {
         $erreur = "Mauvais identifiant ou mot de passe !";
      }
   } else {
      $erreur = "Tous les champs doivent être complétés !";
   }
}
?>
<html>
   <head>
      <title>CONNEXION</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/loginform.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
   <body>
   <div class="login">
      <div class="login-screen">
            <h1><center>Connexion</center></h1>

         <form class="formconnexion" method="POST" action="">
               <center><input id="pseudoconnect" type="text" class="inepoute centerholder" value="" placeholder="Identifiant" name="pseudoconnect" required="required"></center>
               <center><input id="mdpconnect" type="password" class="inepoute centerholder" value="" name="mdpconnect" placeholder="Mot de passe" required="required"></center>
               <br><br>
               <center><h4>Gestion des budgets participatifs</h4></center>
               <center><input id="rememberme" type="checkbox" name="rememberme" id="remembercheckbox" checked="checked" hidden="hidden" />
                  <label style="padding-top: 2.5px;" for="remembercheckbox" hidden="hidden">Se souvenir de moi</label></center>
                  <br>
         <?php
         if(isset($erreur)) {
            echo '<font color="red">'.$erreur."</font>";
         }
         ?>
                  <br><br>
                  <center><input type="submit" id="loggin-btn" class="inepoute" name="formconnexion" value="Se connecter !" /></center>
                  <center><a style="margin: 30px 0;" href="lostpass.php" class="login-link">Mot de passe oublié ?</a></center>
                  <center><a class="inepoute login-link" href="inscription.php">S'inscrire</a></center>
      </div>
   </div>
</body>
</html>