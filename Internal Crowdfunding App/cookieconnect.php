<?php
if(!isset($_SESSION['id']) AND isset($_COOKIE['pseudo'],$_COOKIE['password']) AND !empty($_COOKIE['pseudo']) AND !empty($_COOKIE['password'])) {
   $requser = $bdd->prepare("SELECT * FROM membres WHERE pseudo = ? AND motdepasse = ?");
   $requser->execute(array($_COOKIE['pseudo'], $_COOKIE['password']));
   $userexist = $requser->rowCount();
   if($userexist == 1)
   {
      $userinfo = $requser->fetch();
      $_SESSION['id'] = $userinfo['id'];
      $_SESSION['pseudo'] = $userinfo['pseudo'];
      $_SESSION['mail'] = $userinfo['mail'];
      header("Location: profil.php?id=".$_SESSION['id']);//lance une redirection vers la page 'profil.php' avec dans l'url l'ajout d'une variable de session qui est l'ID correspondant au mail et mdp au dessus
   }
}
?>