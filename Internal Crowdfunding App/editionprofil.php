<?php
session_start();//lancement de la session avec attribution d'un ID perso temporaire

include_once('php/functions.php');

if(isset($_SESSION['id'])) {//si il y'a un ID de session attribué...(cette balise se ferme en bas de page)
   $requser = $bdd->prepare("SELECT * FROM membres WHERE id = ?");//se prépare à aller chercher l'ID en question
   $requser->execute(array($_SESSION['id']));//exécute la comande au dessus
   $user = $requser->fetch();//attribue à 'user' les infos précédemment récupérées
   if(isset($_POST['newpseudo']) AND !empty($_POST['newpseudo']) AND $_POST['newpseudo'] != $user['pseudo']) {//si le champ du formulaire est rempli ET, est différent du pseudo déjà exisant alors...
      $newpseudo = htmlspecialchars($_POST['newpseudo']);//protège le formulaire de toute injection SQL

         $pseudolength = strlen($newpseudo);//strlen calcule le nombre de caracteres de la chaine 'pseudo'
         if($pseudolength <= 15) {//si la taille de 'pseudo' est inférieure ou égale à 15.
         $reqpseudo = $bdd->prepare("SELECT * FROM membres WHERE pseudo = ?");//se prépare à aller chercher si le pseudo est déjà utilisé dans la base de données.
            $reqpseudo->execute(array($newpseudo));//execute l'action au dessus sous forme de tableau
            $pseudoexist = $reqpseudo->rowCount();//compte le nombre de mail dans le tableau
            if($pseudoexist == 0) { //si le mail existe retourne '0'
               $insertpseudo = $bdd->prepare("UPDATE membres SET pseudo = ? WHERE id = ?");//se prépare à mettre à jour la case 'pseudo' de la ligne correspondante à l'ID de l'utilisateur
               $insertpseudo->execute(array($newpseudo, $_SESSION['id']));//exécute la commande au dessus
               header('Location: profil.php?id='.$_SESSION['id']);//redirige vers profil.php avec notre id de session perso
            }else{
               $msg = "Ce Nom, Prénom est déjà utilisé";
            }
         } else {
            $msg = "Votre Nom, Prénom ne doit pas dépasser 15 caractères !";
         }
   }
   
   if(isset($_POST['newmail']) AND !empty($_POST['newmail']) AND $_POST['newmail'] != $user['mail']) {
      $newmail = htmlspecialchars($_POST['newmail']);
      if(filter_var($newmail, FILTER_VALIDATE_EMAIL)) { // si l'adresse mail est vérifiée et valide
         $reqmail = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");//se prépare à aller trouver tous les mails identiques dans la BDD
         $reqmail->execute(array($newmail));//execute l'action au dessus sous forme de tableau
         $mailexist = $reqmail->rowCount();//compte le nombre de mail dans le tableau
         if($mailexist == 0) { //si le mail existe retourne '0'
            $insertmail = $bdd->prepare("UPDATE membres SET mail = ? WHERE id = ?");
            $insertmail->execute(array($newmail, $_SESSION['id']));
            header('Location: profil.php?id='.$_SESSION['id']);
         }else{
            $msg = "Cette adresse mail est déjà utilisée";
            }
      } else {
          $msg = "Votre adresse mail n'est pas valide !";
         }
   }
   if(isset($_POST['newmdp1']) AND !empty($_POST['newmdp1']) AND isset($_POST['newmdp2']) AND !empty($_POST['newmdp2'])) {
      $mdp1 = sha1(htmlspecialchars($_POST['newmdp1']));
      $mdp2 = sha1(htmlspecialchars($_POST['newmdp2']));
      if($mdp1 == $mdp2) {
         $insertmdp = $bdd->prepare("UPDATE membres SET motdepasse = ? WHERE id = ?");
         $insertmdp->execute(array($mdp1, $_SESSION['id']));
         header('Location: profil.php?id='.$_SESSION['id']);
      } else {
         $msg = "Vos deux mdp ne correspondent pas !";
      }
   }


if(isset($_FILES['avatar']) AND !empty($_FILES['avatar']['name'])) {
   $tailleMax = 10485760; //10Mo.
   $extensionsValides = array('jpg', 'jpeg', 'gif', 'png');
   
   if($_FILES["avatar"]["size"] <= $tailleMax) {
      $extensionUpload = strtolower(substr(strrchr($_FILES['avatar']['name'], '.'), 1));
      

      if(in_array($extensionUpload, $extensionsValides)) {
         $chemin = "images/membres/avatars/".$_SESSION['id'].".".$extensionUpload;
         $resultat = move_uploaded_file($_FILES['avatar']['tmp_name'], $chemin);
         
         if($resultat) {
            $updateavatar = $bdd->prepare('UPDATE membres SET avatar = :avatar WHERE id = :id');
            $updateavatar->execute(array(
               'avatar' => $_SESSION['id'].".".$extensionUpload,
               'id' => $_SESSION['id']
               ));
            header('Location: profil.php?id='.$_SESSION['id']);
         } else {
            $msg = "Erreur durant l'importation de votre photo de profil, vérifiez que votre image ne dépasse pas 10 Mo.";
         }



      } else {
         $msg = "Votre photo de profil doit être au format jpg, jpeg, gif ou png";
      }



   } else {
      $msg = "Votre photo de profil ne doit pas dépasser 10 Mo";
   }



}

   // if(isset($_POST['newpseudo']) AND $_POST['newpseudo'] == $user['pseudo'])
   // {
   //    header('Location: profil.php?id='.$_SESSION['id']);
   // }

$bigtitle = "Edition Profil";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/editionprofil.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>
      <div align="center">
         <h2>Edition de mon profil</h2>
         <div align="left">
            <!--<a href="profil.php?id=<?php echo $user['id']; ?>"><i class="fa fa-arrow-left" aria-hidden="true"></i></a><br><br>-->
            <form method="POST" action="" enctype="multipart/form-data" class="under-h2">
               <!--<label>Pseudo :</label>-->
               <input type="text" class="inepoute" name="newpseudo" placeholder="Nouveau Nom, Prénom" value="<?php echo $user['pseudo']; ?>" /><br /><br />
               <!--<label>Mail :</label>-->
               <input type="mail" class="inepoute" name="newmail" placeholder="Nouveau mail" value="<?php echo $user['mail']; ?>" /><br /><br />
               <!--<label>Mot de passe :</label>-->
               <input type="password" class="inepoute" name="newmdp1" placeholder="Nouveau mot de passe"/><br /><br />
               <!--<label>Confirmation - mot de passe :</label>-->
               <input type="password" class="inepoute" name="newmdp2" placeholder="Confirmation du mot de passe" /><br /><br />
               <!--<label>Avatar: (moins de 2 Mo)</label>-->
               <div class="file-upload">
                  <input id="field-image" name="avatar" type="file">
                  <button><!--selection de fichier--></button>
                  <span class="file-info"></span>
              </div>
               <input class="inepoute" id="loggin-btn" type="submit" value="Mettre à jour mon profil !" />
            </form>
            <?php
               if(isset($msg)) {
                   echo '<center><font color="red">'.$msg."</font></center>";
               }
            ?>
         </div>
      </div>
   <?php
include_once('php/pre-bottom.php');
?>
<script>(function ($) {
    "use strict";
    $.fn.prettyUploader = function (options) {

        var settings = $.extend({
                info: '.file-info',
                input: 'input',
                button: 'button',
                buttonNoFileText: 'Photo de profil',
                buttonFileText: 'Changer fichier',
                defaultText: 'Pas d\'image sélectionnée'
            }, options);

        return this.each(function () {
            var $this = $(this),
                input = $this.find(settings.input),
                button = $this.find(settings.button),
                info = $this.find(settings.info),
                infoText = settings.defaultText,
                sizeUnits = [
                    {
                        suffix: 'tb',
                        bytes: 1099511627776
                    },{
                        suffix: 'gb',
                        bytes: 1073741824
                    },{
                        suffix: 'mb',
                        bytes: 1048576
                    },{
                        suffix: 'kb',
                        bytes: 1024
                    }
                ],
                updateText = function() {
                    var files = input[0].files;

                    if( files.length > 0 ) {
                        var file = files[0],
                            name = file.name,
                            size = file.size,
                            sizeSuffix = 'bytes';

                        for (var i = 0; i <= sizeUnits.length; i++){
                            if (size > sizeUnits[i].bytes){
                                size = Math.ceil(size / ( sizeUnits[i].bytes / 100) ) / 100;
                                sizeSuffix = sizeUnits[i].suffix;
                                break;
                            }
                        }

                        infoText = name + ' <em>' + size + sizeSuffix + '</em>';
                    } else {
                        infoText = settings.defaultText;
                    }

                    info.html(infoText);
                },
                updateButton = function() {
                    var files = input[0].files,
                        text = (files.length > 0) ? settings.buttonFileText : settings.buttonNoFileText;

                    button.html(text);
                },
                updateAll = function() {
                    updateText();
                    updateButton();
                };

            $this.on('change', 'input', function() {
                updateAll();
            });

            updateAll();
        });
    };
}(jQuery));

$('.file-upload').prettyUploader();</script>
<?php
include_once('php/bottom.php');//</body>...
?>
<?php  
} else {
   header("Location: connexion.php");
} //(fermeture de la balise du premier 'if')

?>