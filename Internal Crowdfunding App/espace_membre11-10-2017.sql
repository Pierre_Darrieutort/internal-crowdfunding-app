-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: espace_membre
-- ------------------------------------------------------
-- Server version	5.5.55-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `articles`
--

DROP TABLE IF EXISTS `articles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `articles` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) DEFAULT NULL,
  `contenu` text,
  `date_time_publication` datetime DEFAULT NULL,
  `date_time_edition` datetime DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `recolte` float DEFAULT '0',
  `status` tinyint(4) DEFAULT '1',
  `date_time_frozen` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `articles`
--

LOCK TABLES `articles` WRITE;
/*!40000 ALTER TABLE `articles` DISABLE KEYS */;
INSERT INTO `articles` VALUES (23,'Nouvelle salle de repos','Bonjour,\nJe vous propose de réaménager un salle du deuxième étage pour la transformer en nouvelle salle de repos et y ajouter un canapé et une fontaine à eau.\nLe montant serait selon moi de 1500€','2017-06-01 12:53:50','2017-09-11 14:27:36','Teddy Riner',21,50,2,'2017-09-28 19:38:46'),(33,'statue en papier','bla bla','2017-09-11 14:54:56','2017-09-28 19:35:51','Fabrice Richoux',22,0,2,NULL),(34,'statue en marbre',' omgei sgmrsgm','2017-09-11 14:54:56','2017-09-28 19:36:04','Fabrice Richoux',22,0,3,NULL),(35,'deuxiÃ¨me jardin','je vous prÃ©sente idÃ©e bla bla... selon moi, il nous faudrait environ 4500 &amp;hearts;','2017-09-28 19:40:52',NULL,'Pierre Darrieutort',9,0,2,'2017-09-28 19:41:23'),(36,'dfb','sdf','2017-10-08 13:57:25',NULL,'Pierre Darrieutort',9,0,1,NULL),(37,'dfb','sdf','2017-10-08 14:01:29',NULL,'Pierre Darrieutort',9,0,1,NULL),(38,'dfb','sdf','2017-10-08 14:01:50',NULL,'Pierre Darrieutort',9,0,1,NULL),(39,'df','fgh','2017-10-08 14:14:48',NULL,'Pierre Darrieutort',9,0,1,NULL),(40,'df','fgh','2017-10-08 14:14:55',NULL,'Pierre Darrieutort',9,0,1,NULL),(41,'dskjgfh','sdfkljghsdlfkgh','2017-10-09 08:15:16',NULL,'Pierre Darrieutort',9,0,1,NULL),(42,'dskjgfh','sdfkljghsdlfkgh','2017-10-09 08:16:47',NULL,'Pierre Darrieutort',9,0,1,NULL),(43,'dskjgfh','sdfkljghsdlfkgh','2017-10-09 08:20:17',NULL,'Pierre Darrieutort',9,0,1,NULL),(44,'dskjgfh','sdfkljghsdlfkgh','2017-10-09 08:21:04',NULL,'Pierre Darrieutort',9,0,1,NULL),(45,'dskjgfh','sdfkljghsdlfkgh','2017-10-09 08:22:11',NULL,'Pierre Darrieutort',9,0,1,NULL),(46,'slqidufgh','ldusg','2017-10-09 08:23:51',NULL,'Pierre Darrieutort',9,0,1,NULL),(47,'Nouvelle salle -v2 - de repos','Bonjour,\nJe vous propose de réaménager un salle du deuxième étage pour la transformer en nouvelle salle de repos et y ajouter un canapé et une fontaine à eau.\nLe montant serait selon moi de 1500€','2017-10-09 09:15:25','2017-09-11 14:27:36','Teddy Riner',21,50,1,'0000-00-00 00:00:00');
/*!40000 ALTER TABLE `articles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commentaires`
--

DROP TABLE IF EXISTS `commentaires`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commentaires` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `commentaire` text,
  `id_article` int(11) DEFAULT NULL,
  `don` tinyint(4) DEFAULT NULL,
  `amountdon` float DEFAULT NULL,
  `date_time_post` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=86 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commentaires`
--

LOCK TABLES `commentaires` WRITE;
/*!40000 ALTER TABLE `commentaires` DISABLE KEYS */;
INSERT INTO `commentaires` VALUES (84,'Fabrice Richoux',' a fait un don de 5 &euro;',23,1,5,'2017-09-11 14:02:09'),(83,'Teddy Riner','Donnez votre argent !',23,0,NULL,'2017-09-11 00:09:01'),(82,'Teddy Riner',' a fait un don de 20 &euro;',23,1,20,'2017-09-11 00:08:43'),(81,'Pierre Darrieutort','Oui! Super idée.',23,0,NULL,'2017-09-11 00:34:47'),(80,'Pierre Darrieutort',' a fait un don de 25 &euro;',23,1,25,'2017-09-11 00:32:28');
/*!40000 ALTER TABLE `commentaires` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `membres`
--

DROP TABLE IF EXISTS `membres`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `membres` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `mail` varchar(255) DEFAULT '',
  `motdepasse` text,
  `avatar` varchar(255) DEFAULT 'default.jpg',
  `coins` float DEFAULT '30',
  `tokenpass` varchar(34) DEFAULT '',
  `premium` tinyint(1) DEFAULT '1',
  `moderator` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `membres`
--

LOCK TABLES `membres` WRITE;
/*!40000 ALTER TABLE `membres` DISABLE KEYS */;
INSERT INTO `membres` VALUES (9,'Pierre Darrieutort','p.darrieutort@outlook.fr','de271790913ea81742b7d31a70d85f50a3d3e5ae','9.png',5,'pmlUF5sCp154124806201707FeX0YNmrO6',1,0),(22,'Fabrice Richoux','f.richoux@gironde-habitat.fr','7110eda4d09e062aa5e4a390b0a572ac0d2c0220','22.png',20,'Q6yizh354H45135311201709Vi9cJFHcN3',1,1),(21,'Teddy Riner','teddyriner@fauxmail.fr','de271790913ea81742b7d31a70d85f50a3d3e5ae','21.jpg',10,'Z1wDFTLJSX300047112017098fQ4Bhyc1C',1,0),(18,'Mélanie Drouzai','m.drouzai@gironde-habitat.fr','aab33585e9f1128b18023f538f2ff92a28dd4eff','default.jpg',40,'Y0BuNtfpGD05112830201708uoCPDL7rU9',1,0),(19,'Muriel Corporandy ','m.corporandy@gironde-habitat.fr','40bc5cf9c28eb028f49cb0246c4987f63f07128d','default.jpg',100,'vDrJ5g74Ds30151904201709V7aFKRVrQD',1,0),(17,'BANQUE','bank@bank.bank','de271790913ea81742b7d31a70d85f50a3d3e5ae','default.jpg',35,'oirrsCp14774124805201707Feg023mr85',1,0),(20,'Gilles Darrieutort','g.darrieutort@gironde-habitat.fr','54739c82c33818e2bcf24fbf6ab35352cbcc188c','default.jpg',30,'vo3F9p7cNs00190009201709KAcMTrj0x2',1,0);
/*!40000 ALTER TABLE `membres` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `id_expediteur` int(11) DEFAULT NULL,
  `id_destinataire` int(11) DEFAULT NULL,
  `message` text,
  `lu` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (1,4,13,'fraise',0),(2,4,4,'sdfcgsdfg',1),(3,4,10,'zedzdee',0),(11,4,4,'fxg',1);
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `online`
--

DROP TABLE IF EXISTS `online`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `online` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `time` int(11) DEFAULT NULL,
  `user_ip` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `userID` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=1219 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `online`
--

LOCK TABLES `online` WRITE;
/*!40000 ALTER TABLE `online` DISABLE KEYS */;
INSERT INTO `online` VALUES (1218,1507556083,'192.168.33.1','Pierre Darrieutort',9);
/*!40000 ALTER TABLE `online` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shoutbox`
--

DROP TABLE IF EXISTS `shoutbox`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `shoutbox` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(255) DEFAULT NULL,
  `message` text,
  `publidate` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=215 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shoutbox`
--

LOCK TABLES `shoutbox` WRITE;
/*!40000 ALTER TABLE `shoutbox` DISABLE KEYS */;
INSERT INTO `shoutbox` VALUES (60,'aze','mangue','2017-07-20 10:48:12'),(61,'aze','testdate','2017-07-20 10:48:35'),(62,'aze','testdate','2017-07-20 10:48:36'),(63,'aze','testdate','2017-07-20 10:48:36'),(64,'aze','testdate','2017-07-20 10:48:36'),(65,'aze','testdate','2017-07-20 10:48:37'),(66,'aze','testdate','2017-07-20 10:48:37'),(67,'aze','testdate','2017-07-20 10:48:37'),(68,'aze','testdate','2017-07-20 10:48:37'),(69,'aze','testdate','2017-07-20 10:48:38'),(70,'aze','testdate','2017-07-20 10:48:40'),(71,'aze','testdate','2017-07-20 10:48:40'),(72,'aze','testdate','2017-07-20 10:48:40'),(73,'aze','testdate','2017-07-20 10:48:40'),(74,'aze','testdate','2017-07-20 10:48:40'),(75,'aze','testdate','2017-07-20 10:48:41'),(76,'aze','testdate','2017-07-20 10:48:41'),(77,'aze','testdate','2017-07-20 10:48:41'),(78,'aze','testdate','2017-07-20 10:48:41'),(79,'aze','testdate','2017-07-20 10:48:41'),(80,'aze','testdate','2017-07-20 10:48:41'),(81,'aze','testdate','2017-07-20 10:48:42'),(82,'aze','testdate','2017-07-20 10:48:43'),(83,'aze','testdate','2017-07-20 10:48:43'),(84,'aze','testdate','2017-07-20 10:48:44'),(85,'aze','testdate','2017-07-20 10:51:19'),(86,'aze','testdate','2017-07-20 10:51:33'),(87,'aze','testdate','2017-07-20 10:51:40'),(88,'aze','testdate','2017-07-20 10:52:01'),(89,'aze','cghj','2017-07-20 10:52:06'),(90,'aze','bh,','2017-07-20 10:52:08'),(91,'aze','bh,','2017-07-20 10:53:12'),(92,'aze','bh,','2017-07-20 10:53:40'),(93,'aze','bh,','2017-07-20 10:53:53'),(94,'aze','bh,','2017-07-20 10:53:58'),(95,'aze','bh,','2017-07-20 12:09:59'),(96,'aze','xcv','2017-07-20 12:12:41'),(97,'aze','fgh','2017-07-20 12:12:54'),(98,'aze','fgh','2017-07-20 12:14:08'),(99,'aze','fgh','2017-07-20 12:14:12'),(100,'aze','fgh','2017-07-20 12:14:28'),(101,'aze','dfg','2017-07-20 12:14:42'),(102,'aze','cbf','2017-07-20 12:14:47'),(103,'aze','rty','2017-07-20 12:15:44'),(104,'aze','ererg','2017-07-20 12:15:49'),(105,'aze','hjk','2017-07-20 12:16:10'),(106,'aze','fgh','2017-07-20 12:16:17'),(107,'aze','fgh','2017-07-20 12:27:30'),(108,'aze','sdf','2017-07-20 12:27:34'),(109,'aze',':)',NULL),(110,'aze',':)\r\n',NULL),(111,'aze',':)\r\n',NULL),(112,'aze','sdf','2017-07-20 12:48:18'),(113,'aze','sdf','2017-07-20 12:49:53'),(114,'aze','sdf','2017-07-20 12:50:11'),(115,'aze','sdf','2017-07-20 12:56:35'),(116,'aze','sdf','2017-07-20 12:57:26'),(117,'aze','ghjk','2017-07-20 13:05:26'),(118,'aze','ghjk','2017-07-20 13:09:01'),(119,'aze','qsdgdg\r\n','2017-07-20 13:09:14'),(120,'aze','qsdgdg\r\n','2017-07-20 13:10:29'),(121,'aze','dg','2017-07-20 13:10:40'),(122,'aze','dg','2017-07-20 13:11:03'),(123,'aze','dg','2017-07-20 13:13:15'),(124,'aze','dfg','2017-07-20 13:13:19'),(125,'aze','dfjkng','2017-07-20 13:13:24'),(126,'aze','dfjkng','2017-07-20 13:13:24'),(127,'aze','dfjkng','2017-07-20 13:13:24'),(128,'aze','dfjkng','2017-07-20 13:13:24'),(129,'aze','dfjkng','2017-07-20 13:13:24'),(130,'aze','dfjkng','2017-07-20 13:13:24'),(131,'aze','dfjkng','2017-07-20 13:13:24'),(132,'aze','dfjkng','2017-07-20 13:13:24'),(133,'aze','dfjkng','2017-07-20 13:13:24'),(134,'aze','dfjkng','2017-07-20 13:13:24'),(135,'aze','dfjkng','2017-07-20 13:13:24'),(136,'aze','sdf','2017-07-20 13:13:56'),(137,'aze','miam','2017-07-20 13:14:23'),(138,'aze','sdf','2017-07-20 13:14:48'),(139,'aze','sdfkghj','2017-07-20 13:15:21'),(140,'aze','dfogihk','2017-07-20 13:15:27'),(141,'aze','dfghij','2017-07-20 13:15:34'),(142,'aze','sdfkghj','2017-07-20 13:15:39'),(143,'aze','dfghij','2017-07-20 13:15:41'),(144,'aze','dfghij','2017-07-20 13:24:08'),(145,'aze','dfghij','2017-07-20 13:24:18'),(146,'aze','dfghij','2017-07-20 13:24:29'),(147,'aze','dfghij','2017-07-20 13:24:51'),(148,'aze','dfghij','2017-07-20 13:24:58'),(149,'aze','dfghij','2017-07-20 13:25:14'),(150,'aze','dfghij','2017-07-20 13:25:23'),(151,'aze','dfghij','2017-07-20 13:25:34'),(152,'aze','dfghij','2017-07-20 13:25:42'),(153,'aze','dfghij','2017-07-20 13:25:52'),(154,'aze','dfghij','2017-07-20 13:26:34'),(155,'aze','test','2017-07-20 13:26:41'),(156,'aze','la ','2017-07-20 13:26:54'),(157,'aze','sdfkghj','2017-07-20 13:27:05'),(158,'aze','sdlkfgh','2017-07-20 13:27:15'),(159,'aze','la ','2017-07-20 13:27:47'),(160,'aze','la ','2017-07-20 13:27:53'),(161,'aze','sdlkfgh','2017-07-20 13:28:10'),(162,'aze','sdkughj','2017-07-20 13:28:12'),(163,'aze','miam','2017-07-20 13:28:19'),(164,'aze','la ','2017-07-20 13:31:27'),(165,'aze','hjkl','2017-07-20 13:35:47'),(166,'aze','hjkl','2017-07-20 13:36:19'),(167,'aze','hjkl','2017-07-20 13:36:38'),(168,'aze','hjkl','2017-07-20 13:37:37'),(169,'aze','sdkfgjb','2017-07-20 13:37:41'),(170,'aze','idlfughjk','2017-07-20 13:37:50'),(171,'aze','idlfughjk','2017-07-20 13:37:58'),(172,'aze','fxhcgn','2017-07-20 13:44:49'),(173,'aze','sdjgfhdskjhfg','2017-07-20 13:45:03'),(174,'aze','sdfgsdfg','2017-07-20 13:45:14'),(175,'aze','dxkfgj;h','2017-07-20 13:45:26'),(176,'aze','dxkfgj;h','2017-07-20 13:47:39'),(177,'aze','dfgh','2017-07-20 13:47:47'),(178,'aze','dfgh','2017-07-20 13:49:18'),(179,'aze','dfkjgh','2017-07-20 13:49:30'),(180,'aze','idulfgh','2017-07-20 13:49:36'),(181,'aze','idulfgh','2017-07-20 13:51:15'),(182,'aze','djkfhg','2017-07-20 13:51:20'),(183,'aze','djkfhg','2017-07-20 13:53:45'),(184,'aze','djkfhg','2017-07-20 13:55:05'),(185,'aze','djkfhg','2017-07-20 14:00:28'),(186,'aze','djkfhg','2017-07-20 14:01:18'),(187,'aze','tset','2017-07-20 14:01:26'),(188,'aze','dfgjkh','2017-07-20 14:01:35'),(189,'aze','sldkghj','2017-07-20 14:01:41'),(190,'aze','sdkjfgh','2017-07-20 14:01:49'),(191,'aze','test','2017-07-20 14:02:41'),(192,'aze','sldkfhj','2017-07-20 14:02:53'),(193,'aze','sldkfhj','2017-07-20 14:03:09'),(194,'aze','sldkfhj','2017-07-20 14:03:12'),(195,'aze','sdkfjg','2017-07-20 14:03:17'),(196,'aze','sdkfjg','2017-07-20 14:04:57'),(197,'aze','sdxfjhg','2017-07-20 14:05:02'),(198,'aze','sdxfjhg','2017-07-20 14:05:53'),(199,'aze','dfjkng','2017-07-20 13:13:24'),(200,'aze','miam','2017-07-20 14:21:50'),(201,'aze','sxkldjfgh','2017-07-20 14:22:05'),(202,'aze','skldjfgh','2017-07-20 14:22:17'),(203,'aze','ksdufgjh','2017-07-20 14:22:23'),(204,'aze','sldkjfgh','2017-07-20 14:22:30'),(205,'aze','dflkjgh','2017-07-20 14:22:39'),(206,'aze','slkdjhgf','2017-07-20 14:23:45'),(207,'aze','test','2017-07-20 14:23:52'),(208,'aze','lxkfghj','2017-07-20 14:33:50'),(209,'aze',';sdxjfgh','2017-07-20 14:34:01'),(210,'aze','skdjfgh','2017-07-20 14:35:58'),(211,'aze','test','2017-07-20 14:36:05'),(212,'aze','dfkgjh','2017-07-20 14:36:10'),(213,'aze','jwsdhgfkwd','2017-07-21 14:23:28'),(214,'aze','djkfhg','2017-07-21 14:23:35');
/*!40000 ALTER TABLE `shoutbox` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-10-11 10:52:41
