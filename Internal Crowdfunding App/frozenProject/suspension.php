<?php
include_once('../php/functions.php');
session_start();



//utilisé pour obtenir les infos telles que le pseudo, si l'user est moderateur ou si c'est lui qui a rédigé l'article.
$getid = intval($_SESSION['id']);//transforme en chiffre l'id de session (variable devient chiffre).
      $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
      $requser->execute(array($getid));//exécute la commande au dessus
      $userinfo = $requser->fetch();//va chercher et attribue les infos utilisateurs de 'requser' à 'userinfo' dans un tableau affichable avec la commande "var_dump".
      //var_dump($userinfo);


$pseudo = $userinfo['pseudo'];
$moderator = $userinfo['moderator'];








if(isset($_GET['id']) AND !empty($_GET['id'])) {
	$froz_id = htmlspecialchars($_GET['id']);
	$reqOwnArticle = $bdd->prepare('SELECT member_id FROM articles WHERE id = ?');
	$reqOwnArticle->execute(array($froz_id));
	$articleInfo = $reqOwnArticle->fetch();
				
	if( $pseudo === 'Grand Chef' OR $moderator == 1 OR $articleInfo[0] == $_SESSION['id'] ) {
		$froz = $bdd->prepare('UPDATE articles SET status = ?, date_time_frozen = NOW() WHERE id = ?');
		$froz->execute(array(3, $froz_id));
		header("Location: ../article.php?id=".$froz_id);
	} else {
		echo "'authorization' error; please leave this page";
		header( "refresh:5; url=../profil.php" );
	}
} else {
	echo "'id' error; please leave this page";
	header( "refresh:5; url=../profil.php" );
}
?>