<?php
session_start();//permet de lancer une instance temporaire permetant d'ajouter des variables de session

include_once('php/functions.php');



if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
   header("Location: profil.php?id=".$_SESSION['id']);
}//si l'id dans le cookie de session existe ET qu'il est supérieur à zéro, ALORS direction la page de profil d'utilisateur car cela signifie qu'il est déjà connecté alors il n'a pas besoin d'accéder à cette page !




else if(isset($_POST['forminscription'])) { //Retourne TRUE si var existe et a une valeur autre que NULL. FALSE sinon.
   $pseudo = htmlspecialchars($_POST['pseudo']);//htmlspecialchars — Convertit les caractères spéciaux en entités HTML
   $mail = htmlspecialchars($_POST['mail']);
   $mail2 = htmlspecialchars($_POST['mail2']);
   $mdp = sha1(htmlspecialchars($_POST['mdp'])); //sha1 = type d'encodage du mdp
   $mdp2 = sha1(htmlspecialchars($_POST['mdp2']));
   if(!empty($_POST['pseudo']) AND !empty($_POST['mail']) AND !empty($_POST['mail2']) AND !empty($_POST['mdp']) AND!empty($_POST['mdp2'])) { // !empty = différent de vide
      $pseudolength = strlen($pseudo);//strlen calcule le nombre de caracteres de la chaine 'pseudo'
      if($pseudolength <= 30) {//si la taille de 'pseudo' est inférieure ou égale à 15.
         $reqpseudo = $bdd->prepare("SELECT * FROM membres WHERE pseudo = ?");//se prépare à aller chercher si le pseudo est déjà utilisé dans la base de données.
            $reqpseudo->execute(array($pseudo));//execute l'action au dessus sous forme de tableau
            $pseudoexist = $reqpseudo->rowCount();//compte le nombre de mail dans le tableau
            if($pseudoexist == 0) { //si le mail existe retourne '0'
               if($mail == $mail2) { // si mail égal à mail2
                  if(filter_var($mail, FILTER_VALIDATE_EMAIL)) {// si l'adresse mail est vérifiée et valide
                     $reqmail = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");//se prépare à aller trouver tous les mails identiques dans la BDD
                     $reqmail->execute(array($mail));//execute l'action au dessus sous forme de tableau
                     $mailexist = $reqmail->rowCount();//compte le nombre de mail dans le tableau
                     if($mailexist == 0) { //si le mail existe retourne '0'
                        if($mdp == $mdp2) { //si mdp égal à mdp2


#CREATION DU TOKEN DE RECUPERATION START#
                         $time = microtime(true);
                         $micro = sprintf("%06d",($time - floor($time)) * 1000000);
                         $date = new DateTime( date('Y-m-d H:i:s.'.$micro, $time) );
                         $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
                         $length = 10;
                         $charactersLength = strlen($characters);
                         $randomString = '';
                         $randomString2 = '';
                         for ($i = 0; $i < $length; $i++) {
                             $randomString .= $characters[rand(0, $charactersLength - 1)];
                             $randomString2 .= $characters[rand(0, $charactersLength - 1)];
                         }
                         $tokenpass = $randomString.$date->format('sHidYm').$randomString2;
                         #echo $tokenpass;
#CREATION DU TOKEN DE RECUPERATION END#


                           $insertmbr = $bdd->prepare("INSERT INTO membres(pseudo, mail, tokenpass, motdepasse, avatar) VALUES(?, ?, ?, ?, ?)");//ajoute le membre à la table
                           $insertmbr->execute(array($pseudo, $mail, $tokenpass, $mdp, "default.jpg"));//exécute la commande au dessus
                           $success = "Votre compte a bien été créé ! <!--<a href=\"connexion.php\">Me connecter</a>-->";//retourne un message avec la var 'erreur' pour économiser une nouvelle déclaration php
                        } else {
                           $erreur = "Vos mots de passes ne correspondent pas !";
                        }
                     } else {
                        $erreur = "Adresse mail déjà utilisée !";
                     }
                  } else {
                     $erreur = "Votre adresse mail n'est pas valide !";
                  }
               } else {
                  $erreur = "Vos adresses mail ne correspondent pas !";
               }
         } else {
               $erreur = "Ce Nom, Prénom est déjà utilisé";
            }
      } else {
         $erreur = "Votre Nom, Prénom ne doit pas dépasser 30 caractères !";
      }
   } else {
      $erreur = "Tous les champs doivent être complétés !";
   }
}
?>

<html>
   <head>
      <title>Inscription</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/loginform.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
   <body>
      <div align="center">
         <div class="login">
      <div class="inscri-screen">
            <h1><center>Inscription</center></h1>
         
         <form id="inscriptionform" method="POST" action="">
                     <input type="text" placeholder="Votre Nom, Prénom" id="pseudo" name="pseudo" value="<?php if(isset($pseudo)) { echo $pseudo; } ?>" class="inepoute centerholder"/><br>
                     <input type="email" placeholder="Votre mail" id="mail" name="mail" value="<?php if(isset($mail)) { echo $mail; } ?>" class="inepoute centerholder"/><br>
                     <input type="email" placeholder="Confirmez votre mail" id="mail2" name="mail2" value="<?php if(isset($mail2)) { echo $mail2; } ?>" class="inepoute centerholder"/><br>
                     <input type="password" placeholder="Votre mot de passe" id="mdp" name="mdp" class="inepoute centerholder"/><br>
                     <input type="password" placeholder="Confirmez votre mdp" id="mdp2" name="mdp2" class="inepoute centerholder"/><br>
                     <br />
                     <input type="submit" id="loggin-btn" name="forminscription" value="Je m'inscris" />
         <?php
         if(isset($erreur)) {
            echo '<font color="red">'.$erreur."</font>";
         }
         if(isset($success)) {
            echo '<font color="green">'.$success."</font>";
         }
         ?>
                     <br><br>
                     <a class="inepoute login-link" href="connexion.php">connexion</a>
   </div>
         </form>
      </div>
   </body>
</html>