		<?php



//cette page permet d'actualiser la page de tchat en ajax.

include_once('php/functions.php');

		$allmsg = $bdd->query('SELECT * FROM shoutbox ORDER BY id ASC');//sélectionne tous les messages de la BDD dans l'ordre ascendant(ASC) ou descendant(DESC) avec une limite de 5 messages.

		while ($msg = $allmsg->fetch()) {

         $emoji_replace = array(
            ':)',
            ';)',
            ':angry:',
            ':3',
            ":'(",
            ':|',
            ':('
            );
         $emoji_new = array(
            '<img src="images/emojis/emo_smile.png"/>',
            '<img src="images/emojis/emo_wink.png"/>',
            '<img src="images/emojis/emo_angry.png"/>',
            '<img src="images/emojis/emo_cat.png"/>',
            '<img src="images/emojis/emo_cry.png"/>',
            '<img src="images/emojis/emo_noreaction.png"/>',
            '<img src="images/emojis/emo_sad.png"/>'
            );
         $msg['message'] = str_replace($emoji_replace, $emoji_new, $msg['message']);

      ?>
		<b><?= $msg['pseudo']?>: </b><?= $msg['message']?><br>

<?php
			}
?>