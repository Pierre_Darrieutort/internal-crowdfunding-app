<?php
session_start();//permet de lancer une instance temporaire permetant d'ajouter des variables de session

require('php/functions.php');

include_once('cookieconnect.php');


if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
   header("Location: profil.php?id=".$_SESSION['id']);
}//si l'id dans le cookie de session existe ET qu'il est supérieur à zéro, ALORS direction la page de profil d'utilisateur car cela signifie qu'il est déjà connecté alors il n'a pas besoin d'accéder à cette page !





else if(isset($_POST['formlostpass'])) {
   $email = htmlspecialchars($_POST['email']);//sécurisation du champ
   if(!empty($email)) {//si le champ n'est pas vide
      $requser = $bdd->prepare("SELECT * FROM membres WHERE mail = ?");//se prépare à aller chercher dans la BDD les infos du mail
      $requser->execute(array($email));//exécute la commande au dessus
      $userexist = $requser->rowCount();//compte le nombre de rangées existant avec le même mail et mot de passe
      if($userexist < 1) {//si il n'y a pas de mail correspondant
         $erreur = "Ce mail n'existe pas";
      } else if ($userexist == 1) {
         $success = "Vos informations de connexion ont étées récupérées et vont vous être envoyées par mail.";



         $reqtokenpass = $bdd->prepare("SELECT tokenpass FROM membres WHERE mail = ?");//se prépare à aller chercher dans la BDD les infos du mail
         $reqtokenpass->execute(array($email));//exécute la commande au dessus
         $tokenpassinfo = $reqtokenpass->fetch();//va chercher et attribue les infos
         #var_dump($tokenpassinfo[0]);
         $tokenpasslink = $websiteURL.'resetpass.php?tpi='.$tokenpassinfo[0];


require('php/PHPMailer/class.phpmailer.php');

         
 
         $mail = new PHPMailer();
         #$mail->Host = 'smtp.live.com';
         #$mail->SMTPAuth = false;
         #$mail->Port = 25; // Par défaut
          
		$mail->Host = "auth.smtp.1and1.fr";
		 
		//Seulement si le serveur smtp a besoin d'un authentification
		$mail->SMTPAuth = true;
		$mail->Port = 465;
		$mail->Username = "hello@pierredarrieutort.fr";
		$mail->Password = "Pedrodar12";
		$mail->SMTPSecure = 'tls';



         //encodage du mail 
         $mail->CharSet = 'UTF-8';
         // Expéditeur
         $mail->SetFrom('robot@gironde-habitat.fr', 'Gironde Habitat');
         // Destinataire
         $mail->AddAddress($email);
         // Objet
         $mail->Subject = 'Récupération de votre mot de passe';
          
         // Votre message
         $mail->MsgHTML('Si vous souhaitez r&eacute;initialiser votre mot de passe, cliquez sur ce lien: <b><i><a href="'.$tokenpasslink.'">Modifier mon mot de passe ICI<a></b></i><br><br><i>Cet e-mail est automatique, inutile d\'y répondre.</i>');
          
         // Envoi du mail avec gestion des erreurs
         if(!$mail->Send()) {
           echo 'Erreur : ' . $mail->ErrorInfo;
           $erreur = "Il y a un problème interne, Contactez un administrateur.";
           $success = null;
         } else {
           //echo 'Message envoyé !';
         }






      } else {
         $erreur = "Il y a un problème avec ce mail, Contactez un administrateur.";
      }
   } else {
      $erreur = "Tous les champs doivent être complétés !";
   }
}
?>
<html>
   <head>
      <title>Récupération</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/loginform.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
   <body>
   <div class="login">
      <div class="lostpass-screen">
            <h1 style="margin-bottom: 50px;"><center>Récupération</center></h1>

         <form class="formlostpass" method="POST" action="">
               <center><input required="required" id="email" type="email" class="inepoute centerholder" value="" placeholder="Votre adresse mail" name="email"></center>
                  <br>
         <?php
         if(isset($erreur)) {
            echo '<center><font color="red">'.$erreur."</font></center>";
         }
         if(isset($success)) {
            echo '<center><font color="green">'.$success."</font></center>";
         }
         ?>
                  <br><br>
                  <center><input type="submit" id="loggin-btn" class="inepoute" name="formlostpass" value="Envoyer informations" /></center><br><br>
                  <center><a style="margin: 30px 0;" href="lostrest.php" class="login-link">Mail ou Nom, Prénom oubliés ?</a></center>
                  <center><a class="inepoute login-link" href="connexion.php">Retour</a></center>
      </div>
   </div>
</body>
</html>










