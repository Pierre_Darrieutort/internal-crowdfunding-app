<?php
session_start();//permet de lancer une instance temporaire permetant d'ajouter des variables de session

include_once('php/functions.php');
include_once('cookieconnect.php');


if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
   header("Location: profil.php?id=".$_SESSION['id']);
}//si l'id dans le cookie de session existe ET qu'il est supérieur à zéro, ALORS direction la page de profil d'utilisateur car cela signifie qu'il est déjà connecté alors il n'a pas besoin d'accéder à cette page !





if(isset($_POST['formresetrest'])) {

	$mailreset = htmlspecialchars($_POST['mailreset']);
	$pseudoreset = htmlspecialchars($_POST['pseudoreset']);


	if ( !empty($pseudoreset) OR !empty($mailreset) ) {
		if ( !empty($pseudoreset) ) {
			$reqmail = $bdd->prepare("SELECT mail FROM membres WHERE pseudo = ?");//se prépare à aller chercher dans la BDD les infos du mail
			$reqmail->execute(array($pseudoreset));//exécute la commande au dessus
			$mailinfo = $reqmail->fetch();
			$mailresetFinal = $mailinfo[0];
			if ( $mailresetFinal == NULL ) {
				$mailresetFinal = '<center><font color="red">'."Ce Nom, Prénom est inexistant"."</font></center>";
			}
		}

		if ( !empty($mailreset) ) {
			$reqpseudo = $bdd->prepare("SELECT pseudo FROM membres WHERE mail = ?");//se prépare à aller chercher dans la BDD les infos du mail
			$reqpseudo->execute(array($mailreset));//exécute la commande au dessus
			$pseudoinfo = $reqpseudo->fetch();
			$pseudoresetFinal = $pseudoinfo[0];
			if ( $pseudoresetFinal == NULL ) {
				$pseudoresetFinal = '<center><font color="red">'."Cette adresse mail est inexistante"."</font></center>";
			}
		}

	} else {
		$erreur = "Remplissez au moins un champ";
	}
}






?>
<html>
   <head>
      <title>Récupération</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/loginform.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
   <body>
   <div class="login">
      <div class="lostpass-screen" style="margin-top: 45px;">
            <h1 style="margin-bottom: 50px;"><center>Récupération</center></h1>
         <form class="formresetrest" method="POST" action="">
               <center><label for="pseudoreset">Vous cherchez votre mail ?</label></center>
               <center><input id="pseudoreset" type="text" class="inepoute centerholder" value="" name="pseudoreset" placeholder="Entrez votre pseudo ici"></center>
               <center style="color: green;"><?php if(isset($mailresetFinal)) {echo $mailresetFinal; } ?></center>
               <br><br>
               <center><label for="mailreset">Vous cherchez votre pseudo ?</label></center>
               <center><input id="mailreset" type="email" class="inepoute centerholder" value="" name="mailreset" placeholder="Entrez votre mail ici"></center>
               <center style="color: green;"><?php if(isset($pseudoresetFinal)) {echo $pseudoresetFinal; } ?></center>
               <br>
         <?php
         if(isset($erreur)) {
            echo '<center><font color="red">'.$erreur."</font></center>";
         }
         if(isset($success)) {
            echo '<center><font color="green">'.$success."</font></center>";
         }
         ?>
                  <br>
                  <center><input type="submit" id="loggin-btn" class="inepoute" name="formresetrest" value="Envoyer informations" /></center><br>
                  <center><a class="inepoute login-link" href="lostpass.php">Retour</a></center>
                  <center><a class="inepoute login-link" href="connexion.php">Connexion</a></center>
      </div>
   </div>
</body>
</html>