<?php
session_start();
include('../php/functions.php');

if (isset($_SESSION['id']) AND !empty($_SESSION['id'])) {





   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 1 ) {







if (isset($_POST['envoi_message'])) {
	if (isset($_POST['destinataire'],$_POST['message']) AND !empty($_POST['destinataire']) AND !empty($_POST['message'])) {
		$destinataire = htmlspecialchars($_POST['destinataire']);
		$message = htmlspecialchars($_POST['message']);

		$id_destinataire = $bdd->prepare('SELECT id FROM membres WHERE pseudo = ?');
		$id_destinataire->execute(array($destinataire));
		$dest_exist = $id_destinataire->rowCount();

		if($dest_exist == 1 ) {

			$id_destinataire = $id_destinataire->fetch();
			$id_destinataire = $id_destinataire['id'];

			$ins = $bdd->prepare('INSERT INTO messages(id_expediteur,id_destinataire,message) VALUES(?,?,?)');
			$ins->execute(array($_SESSION['id'],$id_destinataire,$message));
			$success = "Votre message a bien été envoyé.";
		} else {
			$erreur = "Cet utilisateur n'existe pas.";
		}
	} else {
		$erreur = "Veuillez compléter tous les champs.";
	}
}

$destinataires = $bdd->query('SELECT pseudo FROM membres ORDER BY pseudo');




if(isset($_GET['r']) AND !empty($_GET['r'])) {
	$r = htmlspecialchars($_GET['r']);
}





?>


<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Envoi de message</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/premium_mi.css">
</head>
<body>
<a id="Retour" href="../premium.php"><i class="fa fatoufe fa-arrow-left" aria-hidden="true"></i>Retour</a><br>
<center style="border-bottom:1px dotted">
<a href="reception.php"><i class="fa fatoufe fa-envelope-square" aria-hidden="true"></i>Boîte de réception</a><br><br>
</center>
<h3>Nouveau message:</h3>
	<form method="POST">
		<center>
			<label>Destinataire: </label>
			<?php if (isset($r)) { ?>
			<label><?= $r ?></label>
			<input type="hidden" name="destinataire" value="<?= $r ?>">
				<?php } else { ?>
				<select name="destinataire">
				<?php while ($d = $destinataires->fetch()) { ?>
				<option><?= $d['pseudo'] ?></option>
				<?php } ?>
				</select>
			<?php } ?>
			<br>
			<textarea id="boite_de_texte" name="message" placeholder="Votre message"></textarea><br>
			<input type="submit" value="Envoyer" name="envoi_message">
			<?php if (isset($erreur)) {echo '<span style="color:red">'.$erreur.'</span>';} ?> 
			<?php if (isset($success)) {echo '<span style="color:green">'.$success.'</span>';} ?> 
		</center>
	</form>	
</body>
</html>

<?php
	} else {
		header('Location: ../premium.php');
	}
} else {
	header('Location: ../premium.php');
}
?>