<?php
session_start();
include('../php/functions.php');

if (isset($_SESSION['id']) AND !empty($_SESSION['id'])) {
if (isset($_GET['id']) AND !empty($_GET['id'])) {
$id_message = intval($_GET['id']);




   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 1 ) {




$msg = $bdd->prepare('SELECT * FROM messages WHERE id = ? AND id_destinataire = ?');
$msg->execute(array($_GET['id'],$_SESSION['id']));
$msg_nbr = $msg->rowCount();
$m = $msg->fetch();




if ($msg_nbr == 0) { echo "Vous n'avez aucun nouveau message.";}
$p_exp = $bdd->prepare('SELECT pseudo FROM membres WHERE id = ?');
$p_exp->execute(array($m['id_expediteur']));
$p_exp = $p_exp->fetch();
$p_exp = $p_exp['pseudo'];



?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Lecture du message #<?= $id_message ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/premium_mi.css">
</head>
<body>
<a id="Retour" href="../premium.php"><i class="fa fatoufe fa-arrow-left" aria-hidden="true"></i>Retour</a><br>
<center style="border-bottom:1px dotted">
<a href="reception.php"><i class="fa fatoufe fa-envelope-square" aria-hidden="true"></i>Boîte de réception</a><br>
<a href="envoi.php?r=<?= $p_exp ?>"><i class="fa fatoufe fa-retweet" aria-hidden="true"></i>Répondre</a><br>
<a href="supprimer_message.php?id=<?= $id_message ?>"><i class="fa fatoufe fa-trash-o" aria-hidden="true"></i>Supprimer ce message</a><br><br>
</center>
<h3>Lecture du message #<?= $id_message ?></h3>

	Message rédigé par&nbsp;<b><?= $p_exp ?></b>:<br><br>
	<?= nl2br($m['message']) ?><br>
	<?php if($msg_nbr == 0) {
		//echo "Erreur durant le traitement du message; Soit le message n'existe pas, soit vous n'êtes pas autorisé à le lire.";
		header('Location: reception.php');
		} ?>
</body>
</html>

<?php


		$lu = $bdd->prepare('UPDATE messages SET lu = 1 WHERE id = ?');
		$lu->execute(array($m['id']));


		} else {
			header('Location: reception.php');
		}

	} else {
		header('Location: ../premium.php');
	}
} else {
	header('Location: ../premium.php');
}

?>