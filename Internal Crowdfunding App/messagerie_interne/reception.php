<?php
session_start();
include('../php/functions.php');

if (isset($_SESSION['id']) AND !empty($_SESSION['id'])) {


   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 1 ) {





$msg = $bdd->prepare('SELECT * FROM messages WHERE id_destinataire = ? ORDER BY id DESC');
$msg->execute(array($_SESSION['id']));
$msg_nbr = $msg->rowCount();


?>

<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Boîte de réception</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="../css/font-awesome.min.css">
	<link rel="stylesheet" href="../css/premium_mi.css">
</head>
<body>
<a id="Retour" href="../premium.php"><i class="fa fatoufe fa-arrow-left" aria-hidden="true"></i>Retour</a><br>
<center style="border-bottom:1px dotted"><a href="envoi.php"><i class="fa fatoufe fa-pencil-square-o" aria-hidden="true"></i>Nouveau message</a><br></center><br>
<h3>Votre boîte de réception:</h3>
<?php
if ($msg_nbr == 0) { echo "Vous n'avez aucun nouveau message.";}
	 while($m = $msg->fetch()) {
		$p_exp = $bdd->prepare('SELECT pseudo FROM membres WHERE id = ?');
		$p_exp->execute(array($m['id_expediteur']));
		$p_exp = $p_exp->fetch();
		$p_exp = $p_exp['pseudo'];
	?>
	 <div class="message">
		<a href="lecture.php?id=<?= $m['id'] ?>">
		<?php if($m['lu'] == 1) { ?>
			<!--<i>[_Lu_]</i>-->
			<i class="fa fatoufe fa-envelope-open" aria-hidden="true"></i>
		 <?php 
			} else if ($m['lu'] == 0) { ?>
			<!--<i>[_Non-lu_]</i>-->
			<i class="fa fatoufe fa-envelope" aria-hidden="true"></i>
		 <?php 
			} 
		 ?>
		<b><?= $p_exp //pseudo ?></b> vous a envoyé un message</a>
	</div>
	<?php } ?>

</body>
</html>

<?php


	} else {
		header('Location: ../premium.php');
	}
} else {
	header('Location: ../premium.php');
}
?>