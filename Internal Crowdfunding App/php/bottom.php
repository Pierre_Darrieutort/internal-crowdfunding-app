    <script type="text/javascript">
        $(function() { //début attendre quand tou est chargé
            $(".burger").click(function() { //début click
                if ($(".main-menu").css("left") == "0px") {
                    $(this).addClass("fa-bars").removeClass("fa-close").removeClass('active');
                    $(".main-menu, .fond-menu").animate({ //début animate
                        "left": "-100%"
                    }, {
                        duration: 500
                    }); //fin animat
                } else {
                    $(this).addClass("fa-close").removeClass("fa-bars").addClass('active');
                    $(".main-menu, .fond-menu").animate({ //début animate
                        "left": 0
                    }, {
                        duration: 500
                    }); //fin animate
                }
            }); //fin click
        }); //fin attendre quand tou est chargé
    </script>
</html>