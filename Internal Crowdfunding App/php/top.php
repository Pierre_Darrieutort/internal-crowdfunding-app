<?php
$miamuser = $bdd->prepare("SELECT coins FROM membres WHERE id = ?");
$miamuser->execute(array($_SESSION['id']));
$miaminfo = $miamuser->fetch();
#var_dump($miaminfo);
$miamcoin = $miaminfo[0];

if ( $_SESSION['id'] == NULL ) {
  header("Location: connexion.php");//redirection de l'utilisateur
}

/*include_once('users_status.php');*/


include('functions.php');

?>

<style type="text/css">
     @font-face {
      font-family: "Trade Gothic LT Std";
      src: url("<?php echo $websiteURL; ?>fonts/TradeGothicLTStd.otf");
    }

   @font-face {
      font-family: "Trade Gothic LT Std Bold";
      src: url("<?php echo $websiteURL; ?>fonts/TradeGothicLTStd-Bold.otf");
    }
</style>






<html lang="fr">
   <head>
      <meta charset="utf-8">
      <title><?= $bigtitle ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/reset.css">
    <link rel="stylesheet" type="text/css" href="css/menu.css">
    <link href="<?= $stylesheet ?>" rel="stylesheet" type="text/css">
    <link rel="icon" type="image/jpeg" href="images/favicon(gh).jpeg"/>

   </head>
   <header>
   <!--<div id="infouserontop"><?= $_SESSION['pseudo']; ?></div>-->
   <div id="thunes"><?= $miamcoin ?><img id="coin_png" src="images/coin(gh).png"></div>
		 <nav class="main-menu" role="menu principal">
     <center>
     <h2 style="color: white !important; border-bottom: dashed; padding-bottom: 13px;">Budget Participatif</h2>
     </center>
     <!--<img style="width: 75px; height: auto; position: absolute; margin-left: calc((100% - 75px) / 2);" src="/images/L12F.png">-->
     <img id="imgburgermenu" style="width: 75px; height: auto; position: absolute; margin-left: calc((100% - 75px) / 2);" src="images/L12F.png">
     <div class="textburgermenu">
  		 <ul>
           <li><a href="actualites.php"><i class="fa ic-menu fa-home" aria-hidden="true"></i>Les projets</a></li>
           <li><a href="redaction.php"><i id="ic1" class="fa ic-menu fa-lightbulb-o" aria-hidden="true"></i>Soumettre une idée</a></li>
           <li><a href="profil.php"><i class="fa ic-menu fa-user" aria-hidden="true"></i>Mon profil</a></li>
           <li><a href="editionprofil.php"><i class="fa ic-menu fa-wrench" aria-hidden="true"></i>Editer mon profil</a></li>
           <li><a href="about.php"><i class="fa ic-menu fa-info-circle" aria-hidden="true"></i>À propos</a></li>
           <li><a href="deconnexion.php"><i class="fa ic-menu fa-power-off" aria-hidden="true"></i>Se déconnecter</a></li>
  		 </ul>
     </div>
		 </nav>
        <div class="forcerhauteur, fond-menu"></div>
        <div class="burger fa fa-bars"></div>
   </header>
   <body>
   