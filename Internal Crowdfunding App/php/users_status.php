<?php

if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {


	if (session_status() == PHP_SESSION_NONE) {
    session_start();#si la session n'est pas démarrée alors: lance la sesion (pour PHP >= 5.4.0);
	}


	include_once('functions.php');


	$userList = $bdd->query('SELECT * FROM online ORDER BY id DESC');

	$temps_session = 15;//15 secondes;
	$temps_actuel = date("U");//nombre de secondes écoulées depuis époque UNIX;
	$user_ip = $_SERVER['REMOTE_ADDR'];
	$pseudo = $_SESSION['pseudo'];
	$userID = $_SESSION['id'];


	$req_ip_exists = $bdd -> prepare('SELECT * FROM online WHERE user_ip = ?');
	$req_ip_exists -> execute(array($user_ip));
	$ip_exists = $req_ip_exists->rowCount();

	if ( $ip_exists == 0 ) {
	   $add_ip = $bdd -> prepare(' INSERT INTO online (user_ip, time, pseudo, userID) VALUES (?, ?, ?, ?) ');
	   $add_ip -> execute(array($user_ip, $temps_actuel, $pseudo, $userID));
	} else {
	   $update_ip = $bdd -> prepare('UPDATE online SET time = ? AND SET pseudo = ? WHERE user_ip = ?');
	   $update_ip -> execute(array($temps_actuel, $pseudo, $user_ip));
	}

	$session_delete_time = $temps_actuel - $temps_session;

	$del_ip = $bdd -> prepare('DELETE FROM online WHERE time < ?');
	$del_ip -> execute(array($session_delete_time));


	$show_users_number = $bdd->query('SELECT * FROM online');
	$users_number = $show_users_number -> rowCount($show_users_number);

}

?>