<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');




if(isset($_GET['id']) AND $_GET['id'] > 0) {//vérification de l'existance de la variable 'id' et qu'elle est strictement supérieure à zéro
   $getid = intval($_GET['id']);//si l'id est modifié dans l'url retourne cette variable sous la forme de nombre
   $requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
   $requser->execute(array($getid));//exécute la commande au dessus
   $userinfo = $requser->fetch();//va chercher et attribue les infos utilisateurs de 'requser' à 'userinfo'


$bigtitle = "Mon profil";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/profil.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>
      <div align="center" id="contnr">
         <h2><?php if($_SESSION['pseudo'] == $userinfo['pseudo']) {
            echo "Mon profil";
            } else {
            $premium = $bdd->prepare('SELECT premium FROM membres where pseudo = ?');
            $premium->execute(array($userinfo['pseudo']));
            $premium = $premium->fetch();
            $premium = $premium[0];

             if($premium == 1){
               echo 'Profil de:&nbsp;<font color="orange">'.$userinfo['pseudo']."</font>";
               }else{
                  echo 'Profil de:&nbsp;<font color="grey">'.$userinfo['pseudo']."</font>";
                  }
               } ?></h2>
         <?php
            if(!empty($userinfo['avatar']))
            {
         ?>
         <img id="avatarpic" style="background: url('images/membres/avatars/<?php echo $userinfo["avatar"] ?>')"><br><br>

         <?php             
            }
         ?>
         <div class="section_info" >


            Prénom, Nom = <?php 
            $premium = $bdd->prepare('SELECT premium FROM membres where pseudo = ?');
            $premium->execute(array($userinfo['pseudo']));
            $premium = $premium->fetch();
            $premium = $premium[0];

             if($premium == 1){
               echo '<font color="orange">'.$userinfo['pseudo']."</font>";
               }else{
                  echo '<font color="grey">'.$userinfo['pseudo']."</font>";
                  } ?>


            <br /><br>
            <a class="lemail" href="mailto:<?= $userinfo['mail']; ?>">
               Mail = <?php echo $userinfo['mail']; ?>
            </a>
            <br /><br>
            <?php
            if(isset($_SESSION['id']) AND $userinfo['id'] == $_SESSION['id']) {//si il ya un id dans l'url strictement supérieur à zéro et que l'ID de l'URL est le même que celui de la session alors... (tout ce qui est dans cette boucle php est relatif à l'utilisateur).
            ?>
            Monnaie = <?php echo $userinfo['coins']; ?> <img src="images/coin(gh).png" style="width:20px;position:relative;top:3.5px;"><br /><br>
            <a class="voirprojet" href="user_articles.php?id=<?= $_SESSION['id'] ?>">Voir mes projets</a>
            <?php
            }//fermeture de la fonction qui affiche des boutons perso pour l'user connecté
            else { ?>
               <a class="voirprojet" href="user_articles.php?id=<?= $userinfo['id'] ?>">Voir les projets de <?= $userinfo['pseudo'] ?></a>
            <?php }
            ?>
            <!--
            <br><br><a href="infos_users.php">Voir les utilisateurs connectés</a>
            <br><br><a href="premium.php">Espace membre</a>
            -->
         </div>
      </div>
<?php
include_once('php/pre-bottom.php');
include_once('php/bottom.php');//</body>...
?>
<?php   
}//termine la vérification de session commencée au 'if(isset($_GET['id']) AND $_GET['id'] > 0) {...'
else{
   header("Location: connexion.php");
}
?>