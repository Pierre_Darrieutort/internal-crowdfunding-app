<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');


$getid = intval($_SESSION['id']);//transforme en chiffre l'id de session (variable devient chiffre).
   	$requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
   	$requser->execute(array($getid));//exécute la commande au dessus
   	$userinfo = $requser->fetch();//va chercher et attribue les infos utilisateurs de 'requser' à 'userinfo' dans un tableau affichable avec la commande "var_dump".
   	//var_dump($userinfo);


$pseudo = $userinfo['pseudo'];
$member_id = intval($_SESSION['id']);
$voirArticle = 0;
$mode_edition = 0;
$postedSuccess = 0;


if(isset($_GET['edit']) && !empty($_GET['edit'])) {
	$mode_edition = 1;
	$edit_id = htmlspecialchars($_GET['edit']);
	$edit_article = $bdd->prepare('SELECT * FROM articles WHERE id = ?');
	$edit_article->execute(array($edit_id));

	if($edit_article->rowCount() == 1){

		$edit_article = $edit_article->fetch();

	}else{
		die("Le projet concerné n'existe pas.");
	}
}

	if (isset($_POST['article_titre'], $_POST['article_contenu'])) {
		if (!empty($_POST['article_titre']) AND !empty($_POST['article_contenu'])){
			
			
				$article_titre = htmlspecialchars($_POST['article_titre']);
				$article_contenu = htmlspecialchars($_POST['article_contenu']);
				$article_titre_count = strlen($article_titre);

			if ($article_titre_count <= 35 ) {

				if($mode_edition == 0){
					$ins = $bdd->prepare('INSERT INTO articles (titre, contenu, pseudo, member_id, date_time_publication) VALUES (?, ?, ?, ?, NOW())');
					$ins->execute(array($article_titre, $article_contenu, $pseudo, $member_id));
					$postedSuccess = 1;
					$success = 'Votre projet a bien été posté!';

					$obtainArticleID = $bdd->prepare('SELECT id FROM articles WHERE member_id = ? ORDER BY date_time_publication DESC LIMIT 1');
					$obtainArticleID->execute(array($member_id));
					$fetchingArticleID = $obtainArticleID->fetch();
					$articleID = $fetchingArticleID[0];
					#var_dump($articleID);
					if ( $voirArticle !=0 ) {
						$voirArticle = 1;
					}
				}else{
					$update = $bdd->prepare('UPDATE articles SET titre = ?, contenu = ?, date_time_edition = NOW() WHERE id = ?');
					$update->execute(array($article_titre, $article_contenu, $edit_id));
					header("Location: article.php?id=".$edit_id);
					$postedSuccess = 1;
					//$success = 'Votre projet a bien été mis à jour!';
				}

			} else {
				$postedSuccess = 0;
				$erreur = "Votre titre doit être inférieur à 35 caractères";
			}


		}	else{
				$postedSuccess = 0;
				$erreur = "Veuillez remplir tous les champs";
			}
}


$bigtitle = "Rédaction/Edition";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/redac.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>

	<div class="ideaform">
	<h2>Mon Idée !</h2>

<?php if ($postedSuccess == 0) : ?>
	<form method="POST">
		<input type="text" maxlength="30" name="article_titre" class="inepoute centerholder" placeholder="Titre"<?php if($mode_edition == 1) { ?> value="<?= 
      $edit_article['titre'] ?>"<?php } ?> /><br />
		<textarea name="article_contenu" class="inepoute" placeholder="Je présente mon idée"><?php if($mode_edition == 1) { ?><?= 
      $edit_article['contenu'] ?><?php } ?></textarea><br />
		<input type="submit" class="ideaenvoi" value="Envoyer">
	</div>
	</form><br>

	<?php

	if(isset($erreur)) {
           echo '<center><font color="red">'.$erreur."</font></center>";
         }
?>
	<?php elseif ($postedSuccess == 1) : ?>
<?php
    if(isset($success)) {
           echo '<center><font color="green">'.$success."</font></center>";
         }



	if (isset($articleID) AND $voirArticle = 1) {echo "<br><a class=inepoute login-link id='viewArticle' href='article.php?id=".$articleID."'>Voir mon projet</a>";} 

?>

	<?php endif; ?>

<?php
include_once('php/pre-bottom.php');
include_once('php/bottom.php');
?>