<?php
session_start();//permet de lancer une instance temporaire permetant d'ajouter des variables de session

include_once('php/functions.php');
include_once('cookieconnect.php');


if(isset($_SESSION['id']) AND $_SESSION['id'] > 0) {
   header("Location: profil.php?id=".$_SESSION['id']);
}//si l'id dans le cookie de session existe ET qu'il est supérieur à zéro, ALORS direction la page de profil d'utilisateur car cela signifie qu'il est déjà connecté alors il n'a pas besoin d'accéder à cette page !




if(isset($_POST['formresetpass'])) {
$tpi = htmlspecialchars( $_GET['tpi']);
$reqtoken = $bdd->prepare("SELECT tokenpass FROM membres WHERE tokenpass = ?");//se prépare à aller chercher dans la BDD les infos du mail
$reqtoken->execute(array($tpi));//exécute la commande au dessus
$tokeninfo = $reqtoken->fetch();

//var_dump($tokeninfo[0]);
$veriftoken = 0;

if ( $tokeninfo[0] == $tpi ) {
   $veriftoken = 1;
   #$success = "Votre clé de modification est valide";
} else {
   $veriftoken = 0;
   #$erreur = "Votre clé de modification est invalide";
}


$mdpreset = sha1(htmlspecialchars($_POST['mdpreset']));//sécurisation du champ
$mdpresetconfirm = sha1(htmlspecialchars($_POST['mdpresetconfirm']));//sécurisation du champ

if (!empty($mdpreset) && !empty($mdpresetconfirm)) {
   //echo "<br>".$mdpreset."<br>";
   //echo $mdpresetconfirm."<br>";
   if ( $veriftoken === 1 ) {
      if ( $mdpreset === $mdpresetconfirm ) {
         $mdpFinal = $mdpresetconfirm;
         $insertmdp = $bdd->prepare(" UPDATE membres SET motdepasse = ? WHERE tokenpass = ? ");
         $insertmdp->execute(array($mdpFinal, $tokeninfo[0]));
         $success = "Mot de passe mis à jour";
      } else {
         $erreur = "Vos mdp sont différents";
      }
   } else {
      $erreur = "Votre clé de modification est invalide";
   }
} else {
   $erreur = "Remplissez tous les champs";
}
}


?>
<html>
   <head>
      <title>Récupération</title>
      <meta charset="utf-8">
      <link rel="stylesheet" href="css/loginform.css">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
   </head>
   <body>
   <div class="login">
      <div class="lostpass-screen" style="margin-top: 70px;">
            <h1 style="margin-bottom: 50px;"><center>Récupération</center></h1>
         <form class="formresetpass" method="POST" action="">
               <center><input id="mdpreset" type="password" class="inepoute centerholder" value="" name="mdpreset" placeholder="Nouveau mdp" required="required"></center><br><br>
               <center><input id="mdpresetconfirm" type="password" class="inepoute centerholder" value="" name="mdpresetconfirm" placeholder="Confirmez mdp" required="required"></center><br>
                  <br>
         <?php
         if(isset($erreur)) {
            echo '<center><font color="red">'.$erreur."</font></center>";
         }
         if(isset($success)) {
            echo '<center><font color="green">'.$success."</font></center>";
         }
         ?>
                  <br>
                  <center><input type="submit" id="loggin-btn" class="inepoute" name="formresetpass" value="Envoyer informations" /></center><br>
                  <center><a class="inepoute login-link" href="connexion.php">Connexion</a></center>
      </div>
   </div>
</body>
</html>