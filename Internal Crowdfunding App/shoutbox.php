<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');

if(!$_SESSION['id']) {
   header("Location: connexion.php");
}

   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] != 1 ) {
      header("Location: premium.php");
   }



   if (isset($_POST['pseudo']) AND isset($_POST['message']) AND !empty($_POST['pseudo']) AND !empty($_POST['message'])) {
      $pseudo = htmlspecialchars($_POST['pseudo']);
      $message = htmlspecialchars($_POST['message']);
      $insertmsg = $bdd->prepare('INSERT INTO shoutbox(pseudo, message, publidate) VALUES(?, ?, NOW())');
      $insertmsg->execute(array($pseudo, $message));
   }



      $bigtitle = "Mon profil";// ICI LE TITRE DE VOTRE PAGE.
      $stylesheet = "css/shoutbox.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
      include_once('php/top.php');//...<body>
      ?>



<div class="conteneur-shoutbox">
   <div id="Allmsg" class="conteneurAllmsg">
      <?php
      #AFFICHAGE DES MESSAGES START
      $allmsg = $bdd->query('SELECT * FROM shoutbox ORDER BY publidate ASC');
      while ($msg = $allmsg->fetch()) {

         $emoji_replace = array(
            ':)',
            ';)',
            ':angry:',
            ':3',
            ":'(",
            ':|',
            ':('
            );
         $emoji_new = array(
            '<img src="images/emojis/emo_smile.png"/>',
            '<img src="images/emojis/emo_wink.png"/>',
            '<img src="images/emojis/emo_angry.png"/>',
            '<img src="images/emojis/emo_cat.png"/>',
            '<img src="images/emojis/emo_cry.png"/>',
            '<img src="images/emojis/emo_noreaction.png"/>',
            '<img src="images/emojis/emo_sad.png"/>'
            );
         $msg['message'] = str_replace($emoji_replace, $emoji_new, $msg['message']);

      ?>
      <b><?= $msg['pseudo'] ?>: </b><?= $msg['message'] ?><br>
      <?php
      }
      #AFFICHAGE DES MESSAGES END
      ?>
   </div>


   <div class="conteneur-post-shoutbox">
      <form method="POST">
         <input type="text" paceholder="Nom, Prénom" name="pseudo" value="<?php if(isset($pseudo)){echo $pseudo;} ?>"><br>
         <textarea type="text" placeholder="message" name="message"></textarea><br>
         <input type="submit" value="Envoyer">
      </form>
   </div>
</div>











   <!--<br><br>
   <center><a class="inepoute login-link" href="premium.php">Retour</a></center>-->


      <?php
      include_once('php/pre-bottom.php');
      ?>
      <script>
            setInterval('load_messages()', 500);
            function load_messages() {
               $('.conteneurAllmsg').load('load_shoutbox_messages.php');//boucle de réinitialisation
            }
         </script>

      <script>
      setInterval('Allmsg()', 500);
      function Allmsg() {
         document.getElementById('Allmsg').scrollTop = document.getElementById('Allmsg').scrollHeight;
      }
      </script>

      <script>
      var lastScrollTop = 0;
$(window).scroll(function(event){
   var st = $(this).scrollTop();
   if (st > lastScrollTop){
       // downscroll code
       console.log('down');
   } else {
      // upscroll code
      console.log('up');
   }
   lastScrollTop = st;
});
      </script>
      <?php
      include_once('php/bottom.php');//</body>...


      if(isset($erreur)) {
         echo '<center><font color="red">'.$erreur."</font><center/>";
      } else if (isset($success)) {
         echo '<center><font color="green">'.$success."</font><center/>";
      }
 ?>