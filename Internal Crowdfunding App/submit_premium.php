<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');



if(!$_SESSION['id']) {
   header("Location: connexion.php");
}//si l'id de session n'existe pas alors effectue une redirection.

   else {
   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 0 ) {

   		$premiumCost = 5;//valeur du compte premium.

        $newPremiumMoney = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
        $newPremiumMoney->execute(array($_SESSION['id']));
        $newPremiumMoney = $newPremiumMoney->fetch();
        $newPremiumMoney = $newPremiumMoney[0];

        $moneyFinalUser = $newPremiumMoney - $premiumCost;

        $newPremium = $bdd->prepare('UPDATE membres SET coins = ? WHERE id = ?');
        $newPremium->execute(array($moneyFinalUser, $_SESSION['id']));

        $BankID = 17;
        $verifmoneyBank = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
        $verifmoneyBank->execute(array($BankID));// l'ID "17" est celui du compte "BANQUE".
        $verifmoneyBank = $verifmoneyBank->fetch();
        $verifmoneyBank = $verifmoneyBank[0];

        $moneyFinalBank = $verifmoneyBank + $premiumCost;
        $UpdateMoneyFinalBank = $bdd->prepare('UPDATE membres SET coins = ? WHERE id = ?');
        $UpdateMoneyFinalBank->execute(array($moneyFinalBank, $BankID));


		$premium = $bdd->prepare('UPDATE membres SET premium = ? WHERE id = ?');
		$premium->execute(array(1, $_SESSION['id']));

		$premium = $bdd->prepare('SELECT premium FROM membres where id = ?');
		$premium->execute(array($_SESSION['id']));
		$premium = $premium->fetch();

		$premium = $premium[0];

	} else {
		header("Location: premium.php");
	}



}




$bigtitle = "Souscrire";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/submit_premium.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>


<div class="conteneur-annonce">



<?php
	if ($premium = 1) {
		header( "refresh:10; url=premium.php" );
		echo "<p class='msg-annonce'>Bravo !<br><br>Vous bénéficiez désormais d'un compte premium.<br><br>Vous allez être redirigé dans 10 secondes</p>";
	} else {
		header( "refresh:10; url=premium.php" );
		echo "<p class='msg-annonce'>Une erreur s'est produite.<br><br>Si le problème persiste, contactez un administrateur.<br><br>Vous allez être redirigé dans 10 secondes</p>";
	}
?>
	




</div>








<?php
include_once('php/pre-bottom.php');//inclusion de jQuery. (évite la double inclusion causant soit une corruption de version, soit une double inclusion faisant crash jQuery.).
include_once('php/bottom.php');//</body>...
?>