<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');



if(!$_SESSION['id']) {
   header("Location: connexion.php");
}//si l'id de session n'existe pas alors effectue une redirection.

   else {
   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 0 ) {

$bigtitle = "Souscrire";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/subscribe.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>





<div class="conteneur-annonce">
	<i id="avantages">Les avantages:</i>
	<div id="slideshow">
		<ul id="sContent"><!--
			--><li><img class="image_carouSub" src="images/carouSub/1-512.png"/></li><!--
			--><li><img class="image_carouSub" src="images/carouSub/2-512.png"/></li><!--
			--><li><img class="image_carouSub" src="images/carouSub/3-512.png"/></li><!--
			--><li><img class="image_carouSub" src="images/carouSub/4-512.png"/></li><!--
			--><li><img class="image_carouSub" src="images/carouSub/5-512.png"/></li>
		</ul>
	</div>

	<br><br>
	<a onClick="pop_up();" id="subscribe" href="#"><i class="fa fa-star" aria-hidden="true"></i>Souscrire<i class="fa fa-star" aria-hidden="true"></i></a>
</div>



<!--
- messagerie privée.
- nom en jaune sur profil et liste des connectés. (OK)
- Shoutbox Access.
- Système d'amis.
- Transfert de fonds. (OK)
-Pour seulement 5 Coins. (OK)
-->




<?php
include_once('php/pre-bottom.php');//inclusion de jQuery. (évite la double inclusion causant soit une corruption de version, soit une double inclusion faisant crash jQuery.).?>
<script>
   function pop_up() {
            var txt;
    if (confirm("L'accès premium requiert 5 Coins, Continuer ?") == true) {
        console.log("on continue.");
        <?php
        $newPremiumMoney = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
        $newPremiumMoney->execute(array($_SESSION['id']));
        $newPremiumMoney = $newPremiumMoney->fetch();
        $newPremiumMoney = $newPremiumMoney[0];

        $moneyFinal = $newPremiumMoney - 5;

        $newPremium = $bdd->prepare('UPDATE membres SET coins =? WHERE id = ?');
        $newPremium->execute(array($moneyFinal, $_SESSION['id']));
        ?>
        document.getElementById('subscribe').href='submit_premium.php';
        
    } else {
        console.log("action annulée.");
        document.getElementById('subscribe').href='#';
    }
}     
</script>
<?php include_once('php/bottom.php');//</body>...

	} else {
		header("Location: premium.php");
	}
}
?>