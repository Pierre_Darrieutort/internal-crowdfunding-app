<?php
session_start();//lancement de session permettant d'ajouter des variables de session

include_once('php/functions.php');




if(!$_SESSION['id']) {
   header("Location: connexion.php");
}//si l'id de session n'existe pas alors effectue une redirection.

   else {
   $verifPremium = $bdd->prepare('SELECT premium FROM membres WHERE id = ?');
   $verifPremium->execute(array($_SESSION['id']));
   $verifPremium = $verifPremium->fetch();

   if ( $verifPremium[0] == 1 ) {

      $bigtitle = "Mon profil";// ICI LE TITRE DE VOTRE PAGE.
      $stylesheet = "css/transfert.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
      include_once('php/top.php');//...<body>
      ?>
            


<div id="contenu_page" style="text-align: center;">

   <h2 id="transfertH2">Transférer des fonds</h2>

<?php

if (isset($_POST['transfert'])) {
   $montantfinalhidden = (float)$_POST['montantfinalhidden'];
   $destiName = $_POST['destiName'];

   if ($destiName == $_SESSION['pseudo']) {
      $erreur = "Vous ne pouvez pas vous envoyer de l'argent.";
   } else {

      $argentsursoncompte1 = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
      $argentsursoncompte1->execute(array($_SESSION['id']));
      $argentsursoncompte1 = $argentsursoncompte1->fetch();
      $argentsursoncompte1 = $argentsursoncompte1[0];


      if ( $montantfinalhidden < 0 ) {
         $erreur = "vous ne pouvez pas mettre de valeur négative!";
      } else if ( $montantfinalhidden == 0 ) {
         echo "<script>console.log('Les transferts de 0,00 &euro; sont absolument inutiles...')</script>";
      } else if ( $montantfinalhidden > $argentsursoncompte1 ) {
         $erreur = "Vous n'avez pas assez de coins!";
      } else {

         $destinataireID = $bdd->prepare('SELECT id FROM membres WHERE pseudo = ?');
         $destinataireID->execute(array($destiName));
         $destinataireIDCount = $destinataireID->rowCount();
         $destinataireID = $destinataireID->fetch();
         $destinataireID = $destinataireID[0];

         if ($destinataireID == NULL) {
            $erreur = "Cet utilisateur n'existe pas.";
         } else if ($destinataireIDCount == 1) {

         $argentsursoncompte2 = $bdd->prepare('SELECT coins FROM membres WHERE id = ?');
         $argentsursoncompte2->execute(array($destinataireID));
         $argentsursoncompte2 = $argentsursoncompte2->fetch();
         $argentsursoncompte2 = $argentsursoncompte2[0];

         $resultatclient1 = $argentsursoncompte1 - $montantfinalhidden;
         $resultatclient2 = $montantfinalhidden + $argentsursoncompte2;

         $idClient1 = $_SESSION['id'];
         $idClient2 = $destinataireID;

         $accountupdate1 = $bdd->prepare('UPDATE membres SET coins = '.$resultatclient1.' WHERE id ='.$idClient1);
         $accountupdate2 = $bdd->prepare('UPDATE membres SET coins = '.$resultatclient2.' WHERE id ='.$idClient2);

         $accountupdate1->execute(array());
         $accountupdate2->execute(array());

         $success = "Le transfert a correctement été effectué.";

         } else {
            $erreur = "Erreur inconnue.";
         }
      }
   }
}


?>

   <form id="montantform" method="POST" onSubmit="if(!confirm('le montant de votre don est de: ' + document.getElementById('amount').value+'€')){return false;}">
         <label for="destiName">Destinataire:</label>
         <input required="required" type="text" class="inepoute" class="centerholder" name="destiName" id="destiName" placeholder="Entrez le nom du destinataire" style="text-align: center;"/>
         <br><br>
         <label for="amount">Entrez une somme:</label>
         <input required="required" type="number" class="inepoute" class="centerholder" name="lemontant" id="amount"  min="1" step="1" placeholder="exemple: 15 &euro;" style="text-align: center;"/>
         <input type="hidden" id="montantfinalhidden" name="montantfinalhidden" value="0">
         <br>
         <input type="submit" class="ideaenvoi" id="envoi_soutien" name="transfert" value="Envoyer" onClick="Montant();">
   </form>

   <br><br>
   <center><a class="inepoute login-link" href="premium.php">Retour</a></center>


      <?php
      include_once('php/pre-bottom.php'); ?>
<script>
   function Montant()
        {
            var montant = document.getElementById("amount").value;
            console.log(montant);
            var montantFinal = parseFloat(montant.replace(",", "."));
            console.log(montantFinal);
            var changehiddenvalue = montantFinal;
            document.getElementById("montantfinalhidden").value = changehiddenvalue;
            console.log(changehiddenvalue);
            if ( montant == "0.00 &euro;" ){
               document.getElementById("amount").value = "";
               return false;
            } else if (montant == "" ) {
               return false;
            } else {
               var confirmation = console.log('Le montant de ce don est de: '+montant+'€');
                var txt;
                    txt = "You pressed OK!";
                    console.log(txt);
            }
        }      
</script>
<?php
      include_once('php/bottom.php');//</body>...


         if(isset($erreur)) {
            echo '<center><font color="red">'.$erreur."</font><center/>";
         } else if (isset($success)) {
            echo '<center><font color="green">'.$success."</font><center/>";
         }

   } else {
      header("Location: premium.php");
      }
      ?>
   </div>
<?php
   }
?>