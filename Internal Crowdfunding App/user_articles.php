<?php
session_start();

include_once('php/functions.php');




if(!$_SESSION['id']) {
   header("Location: connexion.php");
}//si l'id de session n'existe pas alors effectue une redirection.


$articles = $bdd->prepare('SELECT * FROM articles WHERE member_id = ? ORDER BY date_time_publication DESC');
$articles->execute(array($_GET['id']));

   	$projectexists = $articles->rowCount();
   	//echo $projectexists;
	if($projectexists == 0) {
		$noproject = 1;
		//echo "noproject 1";
	} else if ($projectexists > 0) {
		$noproject = 0;
		//echo "noproject 0";
	}

$id_du_membre = $bdd->query('SELECT member_id FROM articles WHERE member_id = '.$_SESSION['id'] );
$member_id = $id_du_membre->fetch();






$getid = intval($_SESSION['id']);//transforme en chiffre l'id de session (variable devient chiffre).
   	$requser = $bdd->prepare('SELECT * FROM membres WHERE id = ?');//se prépare à sélectionner l'id de l'utilisateur connecté a cette session
   	$requser->execute(array($getid));//exécute la commande au dessus




   	$userinfo = $requser->fetch();//va chercher et attribue les infos utilisateurs de 'requser' à 'userinfo' dans un tableau affichable avec la commande "var_dump".
   	//var_dump($userinfo);


$pseudo = $userinfo['pseudo'];

//var_dump($pseudo);echo "<br>";echo "<br>";





$bigtitle = "Actualités";// ICI LE TITRE DE VOTRE PAGE.
$stylesheet = "css/actu.css";// ICI LE CHEMIN DE VOTRE FEUILLE DE STYLE.
include_once('php/top.php');//...<body>
?>
	<!--<a href="redaction.php?id=<?php echo $_SESSION['id']; ?>">Soumettre une idée</a><br><br>-->
	
	<ul class="affichage-container">

	<?php 
		//echo $_GET['id']."<br>".$_SESSION['id'];
		if ($noproject == 1 AND $_GET['id'] == $_SESSION['id']) { echo "<br><br><br><center><i>Vous n'avez proposé aucun projet</i></center>"; }
		else if ($noproject == 1 AND $_GET['id'] !== $_SESSION['id']) { echo "<br><br><br><center><i>Cet utilisateur n'a proposé aucun projet</i></center>"; }
		else if ($noproject == 0 ) { /* ne rien faire */ }
	?>

	  	<?php while($a = $articles->fetch()) { ?>
	  	<?php
	  	$reqpesos = $bdd->prepare('SELECT recolte FROM articles WHERE id = ?');
	  	$reqpesos->execute(array($a['id']));
	  	$userpesos = $reqpesos->fetch();
	  	//var_dump($userpesos);


	  	/*****
		affichage du background color (start)
		*****/
		$status = 1;
		//$finalStatus = 1;
		$reqstatus = $bdd->prepare('SELECT status FROM articles WHERE id = ?');
	  	$reqstatus->execute(array($a['id']));
	  	$status = $reqstatus->fetch();
	  	//var_dump($status);

	  	if ($status[0] == 1) {
	  		$finalStatus = "aff1";} 
	  		else if ($status[0] == 2) {
	  			$finalStatus = "aff2";} 
	  			else if ($status[0] == 3) {
	  				$finalStatus = "aff3";}
	  				//var_dump($finalStatus);
	  	/*****
		affichage du background color (end)
		*****/



	  	?>
      	<li class="affichage <?=$finalStatus;?>">
      		<a class="titlearticle" href="article.php?id=<?= $a['id'] ?>"><?= $a['titre'] ?></a>
		      	<?php if( $pseudo === 'Grand Chef' ): ?>
		      		<a class="boutonmodif" href="redaction.php?edit=<?= $a['id'] ?>"><i class="fa modif_icon fa-pencil" aria-hidden="true"></i><!--(ADMIN)--></a>
		      		<a class="boutonsuppr" href="supprimer_article.php?ma=1&id=<?= $a['id'] ?>" onclick="return(confirm('Cet article va être supprimé'));"><i class="fa modif_icon fa-trash-o" aria-hidden="true"></i><!--(ADMIN)--></a>
	      		<?php  elseif ( $a['member_id'] == $_SESSION['id'] ) : ?>
		      		<a class="boutonmodif" href="redaction.php?edit=<?= $a['id'] ?>"><i class="fa modif_icon fa-pencil" aria-hidden="true"></i></a>
		      		<a class="boutonsuppr" href="supprimer_article.php?ma=1&id=<?= $a['id'] ?>" onclick="return(confirm('Cet article va être supprimé'));"><i class="fa modif_icon fa-trash-o" aria-hidden="true"></i></a>
		      	<?php elseif( $a['member_id'] != $_SESSION['id'] ) : ?>
		      		<!--<i>seul le créateur peut modifier cet article.</i>-->
		      	<?php else : ?>
		      		<!--<i>Vous pouvez modifier vos articles uniquement si vous êtes connectés.</i>-->
	      		<?php endif;	?>
			<right><?php echo  "<div class='lospesos' align='right'>$userpesos[0]<img class='coinpng' src='images/coin.png'></div>"; ?></right>
      </li>
      <?php } ?>
	</ul>

<?php
include_once('php/pre-bottom.php');
include_once('php/bottom.php');//</body>...
?>